#!/usr/bin/python3


def solution1(n):
    """Straightforward solution. Easy to follow and explain."""
    out = []
    for i in range(n+1)[1:]:
        if i % 3 == 0 and i % 5 != 0:
            out.append('Three')
        elif i % 3 != 0 and i % 5 == 0:
            out.append('Five')
        elif i % 3 == 0 and i % 5 == 0:
            out.append('ThreeFive')
        else:
            out.append(i)
    return out


def solution2(n):
    """Same result, but with list comprehension."""
    return ['Three' if (i % 3 == 0 and i % 5 != 0)
            else 'Five' if (i % 3 != 0 and i % 5 == 0)
            else 'ThreeFive' if (i % 3 == 0 and i % 5 == 0)
            else i for i in range(n+1)[1:]]


def solution3(n):
    """Same thing implemented as generator."""
    for i in range(n+1)[1:]:
        if i % 3 == 0 and i % 5 != 0:
            yield 'Three'
        elif i % 3 != 0 and i % 5 == 0:
            yield 'Five'
        elif i % 3 == 0 and i % 5 == 0:
            yield 'ThreeFive'
        else:
            yield i


def solution4(i):
    """Same thing implemented as recursive function."""
    if i == 101:
        print()
        return 0
    else:
        if i % 3 == 0 and i % 5 != 0:
            print('Three', end=" ")
            return solution4(i+1)
        elif i % 3 != 0 and i % 5 == 0:
            print('Five', end=" ")
            return solution4(i+1)
        elif i % 3 == 0 and i % 5 == 0:
            print('ThreeFive', end=" ")
            return solution4(i+1)
        else:
            print(i, end=" ")
            return solution4(i+1)


def test_solution1_len():
    assert(len(solution1(100)) == 100)


def test_solution1_10():
    assert(solution1(100)[10-1] == 'Five')


def test_solution1_15():
    assert(solution1(100)[15-1] == 'ThreeFive')


if __name__ == '__main__':
    print("Solution 1")
    [print(x, end=" ") for x in solution1(100)]

    print("\n\nSolution 2. List comprehension.")
    [print(x, end=" ") for x in solution2(100)]

    print("\n\nSolution 3. Generator")
    [print(x, end=" ") for x in solution3(100)]

    print("\n\nSolution 4. Recursive function.")
    solution4(1)
