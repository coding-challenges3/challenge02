from restart.api import RESTArt
from restart.resource import Resource
from mylibrary.postcodes import format_uk, format_lv

formatters = {'uk': {'function': format_uk, 'sample': 'AA9A 9AB'},
              'lv': {'function': format_lv, 'sample': 'LV-3001'}}

api = RESTArt()


@api.register()
class PostcodeValidator(Resource):
    name = '/'

    def index(self, request):
        return {
            'usage': '/<country code>/<postcode>',
            'supported country codes':
                [key for key, value in formatters.items()]}


@api.register(prefix='/<string:country_code>', pk='<string:postcode>')
class UkPostcodeValidator(Resource):
    name = 'formatter'

    def index(self, request, **kwargs):
        if kwargs['country_code'] not in formatters:
            return {'error': 'unsupported country code',
                    'supported country codes':
                        [key for key, value in formatters.items()]}
        else:
            return {'usage': '/{}/<postcode>'.format(kwargs['country_code']),
                    'sample postcode':
                        formatters[kwargs['country_code']]['sample']}

    def read(self, request, postcode, **kwargs):
        f = formatters[kwargs['country_code']]['function']
        s = formatters[kwargs['country_code']]['sample']
        try:
            formatted = f(postcode)
            return {'valid': True, 'formatted': formatted}
        except Warning:
            return {'valid': False, 'sample postcode': s}
