# challenge

To run the code developed in this assignment, first create python _virtual environment_ and install all requirements.

```bash
which python3 #Output: /usr/bin/python3
mkvirtualenv --python=/usr/bin/python3 challenge02
pip install -r requirements.txt
```


### Task 1

Write a program that prints the numbers from 1 to 100. But for multiples of three print “Three” instead of the number and for the multiples of five print “Five”. For numbers which are multiples of both three and five print “ThreeFive”.

#### Solution for Task 1

I have solved this task using four approaches: List comprehension, Generator, Recursive function and a straightforward solution without any special name.

Run the program
```bash
python3 task1.py
 ```

Run tests:
```bash
pytest task1.py
```

### Task 2

Write a library that supports validating and formatting post codes for UK. The details of which post codes are valid and which are the parts they consist of can be found at [wikipedia](https://en.wikipedia.org/wiki/Postcodes_in_the_United_Kingdom#Formatting). The API that this library provides is your choice.

#### Solution for Task 2

The library has been implemented in `mylibrary/` folder. I have also implemented API service around this library using _RESTArt_.

Run tests:
```bash
pytest task2.py
```

Run the API service using _RESTArt_ - a Python library for building REST APIs:
```bash
restart serve_task2:api
 ```

Access API using browser or curl: http://localhost:5000/

Examples:

* http://localhost:5000/uk
* [http://localhost:5000/uk/SW1W](http://localhost:5000/uk/SW1W)
