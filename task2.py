#!/usr/bin/python3

from mylibrary.postcodes import validate_uk, format_uk


def test_good_formatted_code():
    assert(validate_uk('SW1W 0NY') is True)


def test_malformatted_code():
    assert(validate_uk('SW1W  0NY') is False)


def test_good_malformatted_code_with_formatting():
    assert(validate_uk(format_uk('SW1W  0NY')) is True)


def test_empty_code():
    assert(validate_uk('') is False)


if __name__ == '__main__':
    codes = ['AA9A 9AB',
             'AA9A  9AB ',
             'L1 8 JQ',
             ' AA9A 9AB ',
             ' AA9A  9AB',
             'AA9A 9AB',
             'SW1W 0NY',
             'PO16 7GZ',
             'GU16 7HF',
             'L1 8JQ',
             '',
             'L1 82Q']

    for i, e in enumerate(codes):
        try:
            print("Formatted: '{}'.\t Is valid? {}".format(
                format_uk(e), str(validate_uk(format_uk(e)))))
        except Warning:
            print("Cannot format '{}'".format(e))
