#!/usr/bin/python3

import re


def format_uk(s):
    s = s.strip()
    inward = s[-3:].strip()
    outward = s[:-3].strip()
    if validate_uk("{} {}".format(outward, inward)):
        return "{} {}".format(outward, inward)
    else:
        raise Warning("Invalid post code")


def validate_uk(code):
    regex = re.compile("^([Gg][Ii][Rr] "
                       "0[Aa]{2})|((([A-Za-z][0-9]{1,2})|"
                       "(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|"
                       "(([A-Za-z][0-9][A-Za-z])|"
                       "([A-Za-z][A-Ha-hJ-Yj-y][0-9]?[A-Za-z])))) "
                       "[0-9][A-Za-z]{2})$")
    if re.search(regex, code):
        return True
    else:
        return False


def validate_lv(code):
    regex = re.compile("^LV\-[0-9]{4}$")
    if re.search(regex, code):
        return True
    else:
        return False


def format_lv(s):
    s = s.strip()
    if len(s) == 4:
        digits = s[-4:].strip()
        letters = "LV"
    elif 6 <= len(s) <= 7:
        digits = s[-4:].strip()
        letters = s[:2].strip().upper()
    else:
        raise Warning("Invalid post code")
    if validate_lv("{}-{}".format(letters, digits)):
        return "{}-{}".format(letters, digits)
    else:
        raise Warning("Invalid post code")
